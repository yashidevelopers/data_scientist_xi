### Overview:

The mission is to build and test a CTR(click-through rate) prediction model. Click Through Rate is the ratio of number of clicks and the number of people(Impressions) who visited the advertisement or the web page with advertisement. It is one of the many indicators of the performance of an online advertisement campaign. 

### Submission:

You may use whatever language, tools, and additional libraries that you see fit, as long as the following basic requirements are fulfilled:

1. Commit any changes to a new branch, push, and create a pull request in BitBucket when you are ready to start showing your work. 
2. Make sure that there are sufficient and easy-to-find instructions and documentation contained within the repository so that we can build, run, and test what you've done as appropriate. 
  

The submission should contain the predicted probability of click for each ad impression in the test set using the following format:


```
#!python

id,click
201299801079063376438389,0.692
201360555823203051153664,0.347
201602753138797290189189,0.001 
```


  

Also a document walking us through the modeling process including but not just limited to the following:

1. Clean code to analyze, build, validate and test the models. 
2. A walk through of your data analysis part of the modeling(eg: any visualizations to support your analysis).  
3. A clear description of the choices you made during the process.(eg: why did you choose a particular model or ensemble of multiple models?, why did you choose a particular validation technique?, what are the parameters worth optimizing in the model? etc)  
4. If you plan to do include any feature engineering into the mix, please include documentation for that. 

**Note** :  If you fancy completing this project in a IPyhton notebook or using knitR package (assuming you code in python or R), you are welcome to do.

### What we'll be looking for

This is intentionally a very open ended exercise, so there is no single right or wrong answer.

We will be looking for things like:
1. How you approached and thought through the problem 
2. How well you know and used the features of the programming languages, tools, and libraries you made use of 
3. How professionally "put together" and engineered everything is (is the code clean? etc) 

Remember, though, this is a chance for you to be creative and show off, so let us see what you can do, and try to have some fun with it!

### Input Data:

The sections below outline the content and format of the provided input data for you to use during this exercise.

Keep in mind that as is often the case, the input data as the methods and format of outputs suggested might be imperfect or less than optimal. Don't hesitate to include any suggestions you have on how they could be handled better!

##### File Description:

Here we have two files train.csv and test.csv. Some of the fields/columns are anonymized for privacy reasons. 

1. [train.csv](https://s3.amazonaws.com/data-scientist-test-files/train.csv.gz)
2. [test.csv](https://s3.amazonaws.com/data-scientist-test-files/test.csv.gz)

The following are the columns in the dataset and their descriptions:

* tstamp:                 Timestamp of the event
* id:                     unique identifier to join all the event types
* type:                   event type(for a ‘click’ type=4)
* tag_id:                 source of the event
* bid_price:              The price at which we bid
* clearing_price:         some sources report clearing price and others report winning_ratio
* creative_id:            unique id for the video ad
* ad_id:                  unique id of the order
* campaign_id:            unique id of the campaign
* device_type:            type of the device where the video ad was shown
* domain:                 website/app where the video ad was shown
* domain_type:            type of website/app 
* uid:                    unique id for user
* zip:                    location zip code
* winning_ratio:          the ratio at which we win the bid
* segment:                user segment 

  

**Example Data:**
"timestamp","id","type","tag_id","bid_price","clearing_price","creative_id","ad_id","campaign_id","device_type","domain","domain_type","uid","zip","winning_ratio","segment"
1484081819,2.0100151587844e+23,6,"10",14.5900002,NA,"39","26","3",2,"pollhype.com",0,"840adc11-3d18-4bd1-8221",48322,0.8702,11684
1484081824,2.01546069023847e+23,6,"12",14.5900002,1,"40","26","3",2,"ratatype.com",0,"083b0877-a6c6-4f71-a0a6",48317,NA,13875
1484081826,1.97308339189323e+22,6,"10",14.5900002,NA,"47","26","3",2,"6abc.com",0,"04455dc4-ff1e-4c2e-b509",48044,0.8355,11685
1484081829,1.98972418086777e+22,6,"10",14.5900002,NA,"40","26","3",2,"therealstrategy.com",0,"0f94fba1-d2d0-4d4d-be5d",48178,0.9365,15547
1484081831,2.01544239826026e+23,6,"12",14.5900002,1,"37","26","3",2,"charter.net",0,"dd6f4b70-4555-409a-8dd3-",48446,NA,11685